Layoutizer
==========

A really simple image layout previewer.
Just open the htm file, pick the images, pick the layout, and you're done!

It is designed to be a single file, so all dependencies are embedded on the htm
file, and no network is needed.
